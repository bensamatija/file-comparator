﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;
using System.Security.Cryptography;

//using VB = Microsoft.VisualBasic.FileIO;

namespace FilesComparator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /*
         *
         * Function of this app is to detect same files with different file names.
         *
         */

        // Public VARs:
        string fileExtension = "*.htm";
        string set1Folder = @"C:\Users\Matija\Desktop\0-100short";
        string set2Folder = @"";
        string pathSource = @"C:\Users\Matija\Desktop\0-4240_CENSORED";
        string reportPath = @"C:\Users\Matija\Desktop\0-report.txt";
        string regexPattern = @"[\s\t"",!!#%$&*_+-?\[\]\@^˘~¸°˛`˙´˝¨˝¸|':;<>{}()./\\–-]+";
        string hardcodedFilenameStart = "-000_Filename_";
        string[] set1;
        //string[] set2;
        string[] set1_sorted;// = new string[10];
        //string[] set2_sorted;
        List<string> listSet = new List<string>();
        string[] set1_hashes;
        List<string> results = new List<string>();
        List<List<string>> listCollectionTrimmedWordsFiles = new List<List<string>>();
        List<List<string>> listCollectionCountedCombinations = new List<List<string>>();



        private void PrepareSets()
        {
            set1 = Directory.GetFiles(set1Folder, fileExtension, SearchOption.TopDirectoryOnly);
            //set2 = Directory.GetFiles(set2Folder, fileExtension, SearchOption.TopDirectoryOnly);
            //results = new string[set1.Count()];
        }

        private void ReportToTxt(List<string> array)
        {
            CheckIfFileExists(reportPath);

            using (StreamWriter sw = new StreamWriter(reportPath))
            {
                foreach (string i in array)
                {
                    sw.WriteLine(i);
                }
            }
        }

        /// <summary>
        /// This will print the custom sent object to the txt for report.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="seperator"></param>
        private void PrintObjectToTxt(dynamic listofObj, string seperator)
        {
            CheckIfFileExists(reportPath);

            using (StreamWriter sw = new StreamWriter(reportPath))
            {
                foreach (var obj in listofObj)
                {
                    foreach (var value in obj)
                    {
                        sw.Write(value);
                        sw.Write(seperator);
                    }
                    sw.Write("\n");
                }
            }
        }

        //private void PrintObjectToDebugWindow(dynamic listofObj, string seperator)
        //{
        //    CheckIfFileExists(reportPath);

        //    using (StreamWriter sw = new StreamWriter(reportPath))
        //    {
        //        foreach (var obj in listofObj)
        //        {
        //            foreach (var value in obj)
        //            {
        //                listBox_Report.Items.Add(value);
        //                listBox_Report.Items.Add(seperator);
        //            }
        //            sw.Write("\n");
        //        }
        //    }
        //}

        private void CheckIfFileExists(string reportPath)
        {
            while (!File.Exists(reportPath))
            {
                File.Create(reportPath);
                //System.Threading.Thread.Sleep(1000);
            }
            //System.Threading.Thread.Sleep(5000);
        }

        private void ReadTextFromFile()
        {
            listCollectionTrimmedWordsFiles = new List<List<string>>();

            // Every file in path:
            foreach (string file in set1)
            {
                List<string> listTrimmedWords = new List<string>();

                using (StreamReader sr = new StreamReader(file, Encoding.ASCII))
                {
                    string line;

                    // Read every line:
                    while ((line = sr.ReadLine()) != null)
                    {
                        // Seperate by filter chars:
                        string[] r = SeparateStringLineToWords(line);

                        // Add to list collection:
                        foreach (string word in r)
                        {
                            listTrimmedWords.Add(word);
                        }
                    }
                }

                // Add filename to the list for easier recognation: (it should be on position 0 in array!)
                listTrimmedWords.Add(hardcodedFilenameStart + System.IO.Path.GetFileName(file));

                // add collected list to list of list collection:
                listCollectionTrimmedWordsFiles.Add(listTrimmedWords);
            }

            // Make sort order:
            MakeAlphabetOrder(listCollectionTrimmedWordsFiles);
        }

        private void MakeAlphabetOrder(List<List<string>> listNoOrder)
        {
            List<string> listCombinations = new List<string>();
            List<WordCombinations> listWC = new List<WordCombinations>();

            FileWithWordCombinations FWC = new FileWithWordCombinations();
            List<FileWithWordCombinations> listOfFiles = new List<FileWithWordCombinations>();

            Results01 results01 = new Results01();
            List<Results01> listOfResults01 = new List<Results01>();


            foreach (List<string> file in listNoOrder)
            {
                FWC = new FileWithWordCombinations();

                // Alphabetical sort:
                file.Sort();

                // Add file name for easier recogntion:
                FWC.AddFileName(file[0]);

                // Remove file name from the list of words:
                file.RemoveAt(0);

                // Combine the same words, count them:
                listWC = CombineSameWords(file);

                // Add file
                FWC.AddFile(listWC);
                listOfFiles.Add(FWC);
                //listOfListWC.Add(listWC);
            }

            // Compare each file with the made list:
            listOfResults01 = CompareFileWordCombinations(listOfFiles);

            // Sort match results by % match:
            listOfResults01 = SortMatchResults(listOfResults01);

            // Remove hardcodedFilenameStart from the filename:
            listOfResults01 = RemoveHardcoredFilenameStart(listOfResults01, hardcodedFilenameStart);

            //ReportToTxt(matchResults);
            PrintObjectToTxt(listOfResults01, "\t\t");

            //PrintObjectToDebugWindow(listOfResults01, "\t\t");
        }

        private dynamic CompareFileWordCombinations(List<FileWithWordCombinations> listOfFiles)
        {
            int totaFilelSum = 0;
            int error = 0;
            Results01 results01 = new Results01();
            List<Results01> listOfResults01 = new List<Results01>();
            bool wordMatchFound = false;
            string tempFile2name = "";
            float sumWords1 = 0;
            float sumWords2 = 0;
            float sumWordsMax = 0;

            //Parallel.ForEach(listOfFiles, file1 =>
            foreach (FileWithWordCombinations file1 in listOfFiles)
            {
                //Parallel.ForEach(listOfFiles, file2 =>
                foreach (FileWithWordCombinations file2 in listOfFiles)
                {
                    results01 = new Results01();
                    tempFile2name = file2.GetFileName();

                    if (file1.GetFileName() != file2.GetFileName())     // Don't compare same files
                    {
                        foreach (var word1 in file1.GetListOfWordCombinations())    // Loop all words in file 1
                        {
                            // Add word1 count to total:
                            totaFilelSum = totaFilelSum + word1.count;
                            sumWords1 = sumWords1 + word1.count;

                            foreach (var word2 in file2.GetListOfWordCombinations())    // Loop all words in file 2
                            {
                                // Add word1 count to total:
                                //totaFilelSum = totaFilelSum + word2.count;
                                sumWords2 = sumWords2 + word2.count;

                                if (word1.word == word2.word)       // Compare only same words
                                {
                                    wordMatchFound = true;
                                    error = error + Math.Abs(word1.count - word2.count);
                                }
                                else
                                {
                                    if (wordMatchFound == false)
                                    {
                                        error = error + word2.count;
                                    }
                                    //error = error + word2.count;
                                }
                            }
                            // End of file 2. If not word match found add penality:
                            if (wordMatchFound == false)
                            {
                                //error = error + word1.count;                                
                            }
                            //wordMatchFound = false;
                            //// Add word1 count to total:
                            //totaFilelSum = totaFilelSum + word1.count;


                        }
                        // Write to report:
                        sumWordsMax = Math.Max(sumWords1, sumWords2);
                        string result = error.ToString();
                        //string result = Math.Abs((((float)error / (float)totaFilelSum) * 100.0f) - 100.0f).ToString("0.00");
                        //string result = Math.Abs((((float)error / (float)sumWordsMax) * 100.0f) - 100.0f).ToString("0.00");
                        string r = file1.GetFileName() + "\t\t" + tempFile2name + "\t\t" + result + "\t errors";
                        //matchResults.Add(r);
                        results01.string1 = file1.GetFileName();
                        results01.string2 = tempFile2name;
                        results01.string3 = result;
                        results01.string4 = "error(s)";
                        listOfResults01.Add(results01);

                        // Reset for every file to file comparison:
                        wordMatchFound = false;
                        error = 0;
                        totaFilelSum = 0;
                        result = "";
                        sumWords1 = 0;
                        sumWords2 = 0;
                    }

                }       //); // Use this if using Parallel.ForEach...

            }       //); // Use this if using Parallel.ForEach...

            return listOfResults01;
        }

        private dynamic RemoveHardcoredFilenameStart(List<Results01> listOfResults01, string hardcodedText)
        {
            foreach (Results01 r in listOfResults01)
            {
                if (r.string1.Contains(hardcodedText))
                { r.string1 = r.string1.Replace(hardcodedText, ""); }
                if (r.string2.Contains(hardcodedText))
                { r.string2 = r.string2.Replace(hardcodedText, ""); }
                if (r.string3.Contains(hardcodedText))
                { r.string3 = r.string3.Replace(hardcodedText, ""); }
            }

            return listOfResults01;
        }

        private dynamic SortMatchResults(List<Results01> list)
        {
            //list.OrderBy(x => x.string1);
            list.Sort((x, y) => -1 * x.string3.CompareTo(y.string3));   // -1 * at the beginning for Descending
            return list;
        }

        /// <summary>
        /// Count how many times words repat per file
        /// </summary>
        /// <param name="listNoCombined"></param>
        /// <returns></returns>
        private List<WordCombinations> CombineSameWords(List<string> listNoCombined)
        {
            string lastWord;
            List<string> listCombinations = new List<string>();
            WordCombinations WC = new WordCombinations();
            List<WordCombinations> listWC = new List<WordCombinations>();

            for (int i = 0; i < listNoCombined.Count(); i++)
            {
                // Don't add empty words:
                if (listNoCombined[i] != "")
                {
                    lastWord = listNoCombined[i];

                    // Increase the value:
                    if (i != 0 && lastWord == listNoCombined[i - 1])
                    {
                        WC.count++;
                    }
                    // Add new word:
                    else
                    {
                        WC = new WordCombinations();
                        WC.word = lastWord;
                        WC.count = 1;
                        listWC.Add(WC);
                    }
                }
            }


            return listWC;
        }

        private string[] SeparateStringLineToWords(string line)
        {
            string[] r = Regex.Split(line, regexPattern);
            r = r.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            return r;
        }

        private void CompareHashList()
        {
            for (int i = 0; i <= set1.Count() - 1; i++)
            {
                for (int n = 0; i <= set1.Count() - 1; i++)
                {
                    if (i != n)
                    {
                        if (set1_hashes[i] == set1_hashes[n])
                        {
                            //listBox_Report.Items.Add(i +"\t\t" + n);
                            string r = set1[n] + "\t\t" + set1[i];
                            listBox_Report.Items.Add(r);
                            results.Append(r);
                        }
                    }
                }
            }

            ReportToTxt(results);
        }

        private void CompareProcentMatch()
        {
            float procentLimit = 5;

            foreach (string f1 in set1)
            {
                var file1 = System.IO.Path.GetFileName(f1);

                foreach (string f2 in set1)
                {
                    var file2 = System.IO.Path.GetFileName(f2);

                    // Only check if we are not comparing same file:
                    if (f1 != f2)
                    {
                        // Compare byte by byte: (file names don't affect this)
                        float match = CheckIfFilesAreTheSameAndReportProcentMatch(f1, f2);
                        if (match >= procentLimit)
                        {
                            string r = file1 + "\t\t\t\t" + file2 + "\t\t\t\t" + match + " %";
                            listBox_Report.Items.Add(r);
                            results.Add(r);
                        }
                    }
                }
            }

            ReportToTxt(results);
        }

        private void GenerateHashSet()
        {
            set1_hashes = new string[set1.Count()];

            for (int i = 0; i <= set1.Count() - 1; i++)
            {
                string h = CalculateMD5(set1[i]);
                set1_hashes[i] = h;
            }
        }

        static string CalculateMD5(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        private void CheckArrayForNullValues()
        {
            for (int i = 0; i <= set1_sorted.Count() - 1; i++)
            {
                if (set1_sorted[i] == null)
                {
                    listBox_Report.Items.Add(i);
                }
            }
        }

        private void OrderArrayByFilenameNumber()
        {
            set1_sorted = new string[set1.Count() + 1];

            for (int i = 1; i <= set1.Count(); i++)
            {
                for (int n = 0; n <= set1.Count() - 1; n++)
                {
                    var file1 = System.IO.Path.GetFileNameWithoutExtension(set1[n]);
                    int fileNumber = int.Parse(file1);
                    if (i == fileNumber)
                    {
                        set1_sorted[i] = (set1[n]);
                    }
                }
            }
        }

        private void CompareFiles()
        {
            foreach (string f1 in set1)
            {
                var file1 = System.IO.Path.GetFileName(f1);

                foreach (string f2 in set1)
                {
                    var file2 = System.IO.Path.GetFileName(f2);

                    // Only check if we are not comparing same file:
                    if (f1 != f2)
                    {
                        // Compare byte by byte: (file names don't affect this)
                        if (CheckIfFilesAreTheSame(f1, f2))
                        {
                            listBox_Report.Items.Add(file1 + "\t\t" + file2);
                        }
                    }
                }
            }
        }

        private float CheckIfFilesAreTheSameAndReportProcentMatch(string f1, string f2)
        {
            byte[] fileA = null;
            byte[] fileB = null;
            int equal = 0;
            int different = 0;
            float match = 0;

            try
            {
                fileA = File.ReadAllBytes(f1);
                fileB = File.ReadAllBytes(f2);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message + "\n\n" + e.StackTrace);
            }
            if (fileA != null)
            {
                if (fileB != null)
                {
                    if (f1 != f2)
                    {
                        //if (fileA.Length == fileB.Length)
                        {
                            // Get shortest file lenght so we don't run over the array:
                            int maxAllowedLenght = 0;
                            if (fileA.Length < fileB.Length) { maxAllowedLenght = fileA.Length; }
                            else { maxAllowedLenght = fileB.Length; }

                            for (int i = 0; i < maxAllowedLenght; i++)
                            {
                                if (fileA[i] != fileB[i])
                                {
                                    different++;
                                }
                                else
                                {
                                    equal++;
                                }
                            }

                            // Generate Match Procent %:
                            int total = equal + different;
                            match = (float)equal / (float)total * 100;
                        }
                    }
                }
            }

            return match;
        }

        private bool CheckIfFilesAreTheSame(string f1, string f2)
        {
            byte[] fileA = null;
            byte[] fileB = null;

            try
            {
                fileA = File.ReadAllBytes(f1);
                fileB = File.ReadAllBytes(f2);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message + "\n\n" + e.StackTrace);
            }
            if (fileA != null)
            {
                if (fileB != null)
                {
                    if (fileA.Length == fileB.Length)
                    {
                        for (int i = 0; i < fileA.Length; i++)
                        {
                            if (fileA[i] != fileB[i])
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Replace the line containing selected words with another line
        /// </summary>
        private void ReadFilesAndRewrite()
        {
            // Every file in path:
            foreach (string file in set1)
            {
                string fileName = System.IO.Path.GetFileName(file);

                using (StreamReader sr = new StreamReader(file))
                {
                    using (StreamWriter sw = new StreamWriter(pathSource + "\\" + fileName))
                    {
                        string line;

                        // Read every line:
                        while ((line = sr.ReadLine()) != null)
                        {
                            //if (line.Contains("alt=\"Logo for DS Stalkonstruktion"))
                            if (line.Contains("style=\"FONT-SIZE"))
                            {
                                sw.WriteLine("CENSORED");
                            }
                            //else if (line.Contains("style=\"FONT - SIZE: 34pt\">DS"))
                            //{
                            //    sw.WriteLine("CENSORED");
                            //}
                            else
                            {
                                sw.WriteLine(line);
                            }
                        }
                    }
                }
            }
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.set1Path = tb_compareFolderSet1.Text;
            Properties.Settings.Default.set2Path = tb_compareFolderSet2.Text;
            Properties.Settings.Default.fileExtension = tb_filesExtension.Text;
            Properties.Settings.Default.reportFile = tb_reportFile.Text;
            Properties.Settings.Default.Save();
        }

        private void LoadSettings()
        {
            tb_compareFolderSet1.Text = Properties.Settings.Default.set1Path;
            set1Folder = Properties.Settings.Default.set1Path;
            tb_compareFolderSet2.Text = Properties.Settings.Default.set2Path;
            set2Folder = Properties.Settings.Default.set2Path;
            tb_filesExtension.Text = Properties.Settings.Default.fileExtension;
            fileExtension = Properties.Settings.Default.fileExtension;
            tb_reportFile.Text = Properties.Settings.Default.reportFile;
            reportPath = Properties.Settings.Default.reportFile;
        }

        /// <summary>
        /// Clean the report window
        /// </summary>
        private void ClearListboxReportWindow()
        {
            listBox_Report.Items.Clear();
        }

        private void btn_Compare_Click(object sender, RoutedEventArgs e)
        {
            ClearListboxReportWindow();
            listBox_Report.Items.Add("=== Compare Bytes ===");
            PrepareSets();
            CompareFiles();
            listBox_Report.Items.Add("=== End ===");
        }

        private void btn_Count_Click(object sender, RoutedEventArgs e)
        {
            ClearListboxReportWindow();
            listBox_Report.Items.Add("=== Count ===");
            PrepareSets();
            OrderArrayByFilenameNumber();
            CheckArrayForNullValues();
            listBox_Report.Items.Add("=== End ===");
        }

        private void btn_CompareHash_Click(object sender, RoutedEventArgs e)
        {
            ClearListboxReportWindow();
            listBox_Report.Items.Add("=== Compare Hash ===");
            PrepareSets();
            GenerateHashSet();
            CompareHashList();
            listBox_Report.Items.Add("=== End ===");
        }

        private void btn_ComputeProcentMatch_Click(object sender, RoutedEventArgs e)
        {
            ClearListboxReportWindow();
            listBox_Report.Items.Add("=== Compute same byte order procent match ===");
            PrepareSets();
            CompareProcentMatch();
            listBox_Report.Items.Add("=== End ===");
        }

        private void btn_WordComparator_Click(object sender, RoutedEventArgs e)
        {
            ClearListboxReportWindow();
            listBox_Report.Items.Add("=== Compare number of same words between files and display error value ===");
            PrepareSets();
            ReadTextFromFile();
            listBox_Report.Items.Add("=== End ===");
        }

        private void btn_SearchKeywordsInFiles_Click(object sender, RoutedEventArgs e)
        {
            PrepareSets();
            ReadFilesAndRewrite();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSettings();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveSettings();
        }
    }
}
