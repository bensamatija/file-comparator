﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilesComparator
{
    /// <summary>
    /// Words and number of times they repeat
    /// </summary>
    class WordCombinations
    {
        public string word;
        public int count;
    }

    class FileWithWordCombinations
    {
        string fileName;
        List<WordCombinations> listOfWords;


        public void AddFileName(string name)
        {
            fileName = name;
        }

        public List<WordCombinations> GetListOfWordCombinations()
        {
            return listOfWords;
        }

        public string GetFileName()
        {
            string n = fileName;
            return n;
        }

        /// <summary>
        /// List of WC
        /// </summary>
        /// <param name="file"></param>
        public void AddFile(List<WordCombinations> file)
        {
            listOfWords = file;
        }

        public FileWithWordCombinations()
        {
            // Empty Constructor
        }
    }

    /// <summary>
    /// Object for storing report results
    /// </summary>
    class Results01 : IEnumerable<string>   // This allows us to make a foreach loop on the object
    {
        public string string1;
        public string string2;
        public string string3;
        public string string4;

        //public string String1 { get { return string1; } set { string1 = value; } }

        public IEnumerator<string> GetEnumerator()      // This allows us to make a foreach loop on the object
        {
            yield return string1;
            yield return string2;
            yield return string3;
            yield return string4;
        }

        IEnumerator IEnumerable.GetEnumerator()     // This allows us to make a foreach loop on the object
        {
            return GetEnumerator();
        }
    }
}
